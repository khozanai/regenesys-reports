<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
$id = required_param('id', PARAM_INT); // Course ID

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST); // ... course object
$context = context_course::instance($course->id);
require_login($course);

$PAGE->set_title('Reports: ' . $course->shortname);
$PAGE->set_pagelayout('standard');
$PAGE->set_url(new moodle_url('/blocks/regenesysreports/students.php', array(
    'id' => $course->id
)));
$PAGE->navbar->add('Students personal data', new moodle_url('/blocks/regenesysreports/students.php', array(
    'id' => $course->id
)));

echo $OUTPUT->header();

$students = $DB->get_records('local_updatedetails');

$table = new html_table();
$table->head = array('Count', 'Fullname', 'Nationality', 'ID/ Passport', 'E-mail', 'Cellphone', 'Workphone');
$counter = 0;

foreach ($students as $student) {
    $user = $DB->get_record('user', ['id' => $student->userid]);
    $counter++;
    $row = new html_table_row(array(
        $counter,
        fullname($user),
        $student->country,
        $student->id_passport,
        $student->email_address,
        $student->cellphone,
        $student->work_number
    ));
    $row->attributes['class'] = '';
    $row->attributes['id'] = '';
    $table->data[] = $row;
}
echo html_writer::table($table);

echo $OUTPUT->footer();


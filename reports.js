/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require(['jquery'], function ($) {
    $(document).ready(function () {

        /**
         * Download funtionality for the course assessments
         */
        $('#download').click(function () {
            var courseid = $('#courseid').attr('class');
            $(location).attr('href', 'assessments-download.php?id=' + courseid);
        });
        /**
         * The download button for the course that has been archived already. 
         * 
         * There's data from both the ACR and the archives
         */
        $('#download_2').click(function () {
            var courseid = $('#courseid').attr('class');
            var archived = $('#archived').attr('class');
            // alert(archived);
            $(location).attr('href', 'assessments-download.php?id=' + courseid + '&archived=' + archived);
        });

        $('.quiz').click(function () {
            var quizid = $(this).attr('id');
            // document.getElementById('data').innerHTML = quizid;
            $.post('scripts/da.php', {quizid: quizid}, function (success) {
                if (success) {
                    document.getElementById('data').innerHTML = success;
                }
            });


        });


    });
});

<?php

/**
 *  block_regenesysreports
 *
 *  View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

/**
 * 
 * @global stdClass $DB
 * @param type $course
 * @return array of enrolled students in this course
 */
function fetch_enrolled_students($course) {
    global $DB;
    return $students = $DB->get_records_sql('select ue.userid from {enrol} e join {user_enrolments} ue on ue.enrolid = e.id '
            . 'where e.courseid = ? and e.roleid = ?', [$course, 5]);
}

/**
 * 
 * @global stdClass $DB
 * @param type $courseid
 * @param type $userid
 * @param type $contextid
 * @return stdClass with the user enrolment data - need the data here
 */
function dateofenrolment($courseid, $userid, $contextid) {
    global $DB;
    $log = $DB->get_record('logstore_standard_log', [
        'relateduserid' => $userid,
        'courseid' => $courseid,
        'contextid' => $contextid,
        'origin' => 'ws',
        'action' => 'created'
            // 'target' => $DB->sql_compare_text('user_enrolment')
    ]);
    return $log;
}

function enrolment_date($userid, $contextid) {
    global $DB;
    return $record = $DB->get_record('role_assignments', ['contextid' => $contextid, 'userid' => $userid, 'roleid' => 5]);
}

/**
 * 
 * @global stdClass $DB
 * @param type $userid
 * @param type $courseid
 * @return int count of digital assessments done by the student in the course
 */
function count_digitalassessments_done($userid, $courseid) {
    global $DB;

    $grades = $DB->get_records_sql('select count(gg.finalgrade) as count from {grade_items} gi join {grade_grades} gg on gg.itemid = gi.id '
            . 'where gi.courseid = ? and gg.userid = ? and gi.itemmodule = ? and gg.finalgrade is not null order by count desc', [
        $courseid, $userid, 'quiz'
    ]);
    foreach ($grades as $grade) {
        return $grade->count;
    }
}

/**
 * 
 * @global stdClass $DB
 * @param type $course
 * @return array of the digital assessments in the course
 */
function digitalassessments($course) {
    global $DB;
    return $das = $DB->get_records('course_modules', ['course' => $course, 'module' => 16]);
}

/* * Return int count of the digital assessments in the course
 * 
 * @global stdClass $DB
 * @param type $course
 * @return int count of the digital assessments in the course
 */

function count_digitalassessments($course) {
    global $DB;
    $quizzes = $DB->get_records_sql('SELECT * FROM {course_modules} WHERE '
            . 'course = ? AND module = ? AND idnumber like ?', [
        $course,
        16,
        '%DA_%'
    ]);
    $counter = 0;
    foreach ($quizzes as $quiz) {
        $counter++;
    }
    return $counter;
}

/**
 * Return a count of the students who have grades for this quiz
 * @global stdClass $DB
 * @param int $quizid
 * @return int count of the students who have grades for this quiz
 */
function count_students_with_quizgrades($course, $quizid) {
    global $DB;
    $records = $DB->get_records('quiz_grades', ['quiz' => $quizid]);
    $count = 0;
    foreach ($records as $record) {
        $count++;
    }
    return $count;
}

/**
 * Return count of the students who dont have grades for this quiz
 * @global type $DB
 * @param type $course
 * @param type $quizid
 * @return int count of the students who dont have grades for this quiz
 */
function count_students_without_quizgrades($course, $quizid) {
    global $DB;
    $records = $DB->get_records_sql('SELECT gg.userid FROM {grade_items} gi JOIN {grade_grades} gg ON gg.itemid = gi.id '
            . 'WHERE gi.itemmodule = ? AND gi.courseid = ? AND gi.iteminstance = ? AND gg.finalgrade IS NULL', [
        'quiz',
        $course,
        $quizid
    ]);
    $count = 0;
    foreach ($records as $record) {
        $count++;
    }
    return $count;
}

/**
 * grades for this quiz
 * @global type $DB
 * @param type $quizid
 * @return array of grades for this quiz
 */
function fetch_quiz_grades($quizid) {
    global $DB;
    return $grades = $DB->get_records('quiz_grades', ['quiz' => $quizid]);
}

function fetch_da_data() {
    global $DB;

    $total = '';

    $query = 'SELECT * FROM {grade_items} gi JOIN {grade_grades} gg ON gg.itemid = gi.id WHERE '
            . 'gi.courseid = ? AND gi.itemmodule = ?';
}

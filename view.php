<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
$id = required_param('id', PARAM_INT); // Facilitator userid
$courseid = required_param('courseid', PARAM_INT); // course id coming from
$p_forum = optional_param('programme_forum', '', PARAM_INT);
$c_forum = optional_param('course_forum', '', PARAM_INT);

$user = $DB->get_record('user', array('id' => $id), '*', MUST_EXIST); // ... user object
$course = get_course($courseid);
$context = context_course::instance($course->id);


if ($p_forum) {
    $page_url = new moodle_url('/blocks/regenesysreports/view.php', [
        'id' => $id,
        'courseid' => $course->id,
        'programme_forum' => $p_forum
    ]);
    if ($course->id <= 2) {
        $PAGE->navbar->add('Courses', '/');
    }
    $programme_forum = $DB->get_record('programforums', ['id' => $p_forum]);
    $PAGE->navbar->add('Posts', new moodle_url('/blocks/regenesysreports/facilitator_posts.php', [
        'id' => $user->id,
        'courseid' => $course->id
    ]));
    $PAGE->navbar->add('Programme forum post: ' . $programme_forum->title);
} else {
    $page_url = new moodle_url('/blocks/regenesysreports/view.php', [
        'id' => $id,
        'courseid' => $course->id,
        'course_forum' => $c_forum
    ]);
    $course_forum = $DB->get_record('course_forums', ['id' => $c_forum]);
    $PAGE->navbar->add('Course forum post: ' . $course_forum->subject);
}
require_login($course);
$PAGE->set_url($page_url);
$PAGE->set_title('View post');

echo $OUTPUT->header();
$br = html_writer::empty_tag('br');
if ($programme_forum) {
    echo $programme_forum->message;

    $out = array();
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $programme_forum->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $programme_forum->id . '/' . $filename;
        $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
        $out[] = html_writer::link($url, $filename) . $br . $br;
    }
    echo '<span style="font-weight: bold;">' . implode($out) . '</span>';
} else if ($course_forum) {
    echo $course_forum->forum_message;

    $out = array();
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'block_program_forums', 'message', $course_forum->id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
    foreach ($files as $file) {
        $filename = $file->get_filename();
        $path = '/' . $context->id . '/' . 'block_program_forums' . '/' . 'message' . '/' . $course_forum->id . '/' . $filename;
        $url = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
        $out[] = html_writer::link($url, $filename) . $br . $br;
    }
    echo '<span style="font-weight: bold;">' . implode($out) . '</span>';
}

echo $OUTPUT->footer();

<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
$id = required_param('id', PARAM_INT); // Facilitator user id
$courseid = required_param('courseid', PARAM_INT); // course id coming from

$user = $DB->get_record('user', array('id' => $id), '*', MUST_EXIST); // ... user object
$course = get_course($courseid);
$context = context_course::instance($course->id);
require_login($course);
$page_url = new moodle_url('/blocks/regenesysreports/facilitator_posts.php', [
    'id' => $user->id,
    'courseid' => $course->id
        ]);
$PAGE->set_title('Facilitator posts');
$PAGE->set_url($page_url);

if ($course->id <= 2) {
    $PAGE->navbar->add('Courses', '/');
}
$PAGE->navbar->add('Facilitator posts', 'facilitators.php?id=' . $course->id);

echo $OUTPUT->header();

$programme_forums = $DB->get_records_sql("SELECT * FROM {programforums} WHERE createdby = ? ORDER BY id DESC", [$user->id]);
$course_forums = $DB->get_records('course_forums', ['createdby' => $user->id]);


echo '<div style="font-weight: bold; font-size: 18px; margin: 7px 20px;">'
 . 'Course forum messages appear at the bottom of the page'
 . '</div>';

$br = html_writer::empty_tag('br');
$count = 0;
$table = new html_table();
// $table->caption = fullname($user) . ': posts to students';
// $table->head = array('Programme posts', 'Course posts');
$pf_headline_row = new html_table_row([// programme forums heading
    '<h4>Count</h4>',
    '<h4>Target</h4>',
    '<h4>Programme forums</h4>',
    '<h4>Date</h4>'
        ]);
$table->data[] = $pf_headline_row;
foreach ($programme_forums as $programme_forum) { // programme forums display
    $count++;
    $target = $DB->get_record('course_categories', ['id' => $programme_forum->categoryid]);
    $link = 'view.php?id=' . $user->id . '&courseid=' . $course->id . '&programme_forum=' . $programme_forum->id;
    $pf_row = new html_table_row(array(
        $count,
        html_writer::link($link, $target->name),
        html_writer::link($link, $programme_forum->title),
        date('d-F-Y H:i:s: A', $programme_forum->timecreated)
    ));
    $table->data[] = $pf_row;
}

$count = 0;
$cf_headline_row = new html_table_row([// Course forums heading
    '<h4>Count</h4>',
    '<h4>Target</h4>',
    '<h4>Course forums</h4>',
    '<h4>Date</h4>'
        ]);
$table->data[] = $cf_headline_row;

foreach ($course_forums as $course_forum) { // Course forums display
    $count++;
    $target = get_course($course_forum->courseid);
    $link = 'view.php?id=' . $user->id . '&courseid=' . $course_forum->courseid . '&course_forum=' . $course_forum->id;
    $cf_row = new html_table_row([
        $count,
        $target->shortname,
        html_writer::link($link, $course_forum->subject),
        date('d-F-Y H:i:s:A', $course_forum->timecreated)
    ]);
    $table->data[] = $cf_row;
}

echo html_writer::table($table);


echo $OUTPUT->footer();

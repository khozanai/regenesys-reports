<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. names 
 */

$string['pluginname'] = 'Regenesys reports';
$string['regenesysreports'] = 'Regenesys reports';
$string['das'] = 'Digital assessments';
$string['fontawesome'] = 'Font awesome';
$string['third_link'] = 'Third link';
$string['view_coursedata'] = 'View archived data';
$string['archive_course'] = 'Archive a course';
$string['auth_text'] = 'Enter the authorization code below';
$string['auth_input'] = 'submit code';
$string['save_marks'] = 'Archive';
$string['unenrol_students'] = 'Unenrol students who have passed the module';

$string['search'] = 'Search archived courses';
$string['active_students'] = 'Active students';
$string['archived_students'] = 'Removed students';
$string['general'] = 'Some general info about this course archive';
$string['archivename'] = 'Archive or Semester title';
$string['intro'] = 'Description or a clarifying note ';
$string['quiz_data'] = 'Digital/ Exam assessments';
$string['archive_quizzes'] = 'Archive quizzes';
$string['quizzes'] = 'Digital Assessmets & Exams';
$string['quiz-instances'] = 'Archive the rest of the quiz data.';
$string['close-quizmodal'] = 'Close this modal';

$string['turnitin_data'] = 'Turnitin assessments';
$string['archive_turnitin_assignment'] = 'Archive this assignment';
$string['close-turnitinmodal'] = 'Close this modal';

$string['peerassessment_data'] = 'Peer Assessments';
$string['close-peerassessmentmodal'] = 'Close this modal';
$string['archive_peerassessment'] = 'Archive this Peer Assessment';
$string['peerassessment_grades'] = 'grades';

$string['questionnaire_data'] = 'Questionnaires, Ratings and Student feedback data';

$string['uniqueid'] = 'Archive unique id ';
$string['uniqueid_help'] = 'Please set the unique id for this archiving instance. Its needed to uniquely identify each archiving process.';

$string['archive_forums'] = 'Archive course forums';
$string['close-forumsmodal'] = 'Close dialog box';
$string['archive_programme_forum'] = 'Archive';
$string['programme-forums'] = 'Programme Forums';
$string['course-forums'] = 'Course Forums';
$string['course-discussion'] = 'Course Discussions';
$string['archive_course_forum'] = 'Archive';
$string['archive_course_discussion'] = 'Archive';
$string['archive_dean_message'] = 'Archive';
$string['archive_dean_message'] = 'Archive';
$string['dean-messages'] = 'Dean\'s Messages';

$string['defaulttext'] = 'Archive course data';
$string['defaulttitle'] = 'Regenesys reports';
$string['blockstring'] = 'ARegenesys reports';
$string['headerconfig'] = 'Regenesys reports';
$string['descconfig'] = 'Regenesys reports';
$string['labelallowhtml'] = 'Regenesys reports';
$string['descallowhtml'] = 'Regenesys reports';
$string['regenesys:addinstance'] = 'Add a course archive block';
$string['regenesys:myaddinstance'] = 'Add a new Regenesys course archive block to the my moodle page';

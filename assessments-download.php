<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once($CFG->dirroot . '/local/academicrecord/lib.php');
require_once($CFG->dirroot . '/blocks/archivedata/lib.php');
$id = required_param('id', PARAM_INT); // Course ID
$archived = optional_param('archived', FALSE, PARAM_BOOL); // Course ID

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST); // ... course object
$context = context_course::instance($course->id);
require_login($course);

$pageurl = new moodle_url('/blocks/regenesysreports/assessments-download.php', array(
    'id' => $course->id
        ));
$PAGE->set_url($pageurl);

if (!$archived) {

    // HEADERS
    // $filename = 'filename=examvenudata.xls';
    $filename = 'filename=' . $course->shortname . ' student assessments.xls';
    header("Content-Type: application/xls");
    header("Content-Type: application/vnd.ms-excel");
    // header("Content-Type: application/octet-stream");
    header("Content-disposition: attachment; " . basename($filename));

    $students = $DB->get_records_sql("SELECT userid FROM {role_assignments} WHERE contextid = ? AND roleid = ? ", [$context->id, 5]);

    $table = new html_table();
    $table->head = array('Count', 'Student', 'Assignment', 'Digital Assessments', 'Groupwork', 'Exam', 'Final', 'Symbol', 'Result', 'Date enrolled');
    $c = 0; // count of each student
    echo 'Count' . "\t" . 'Student' . "\t" . 'Assignment' . "\t" . "Digital As" . "\t" . 'Groupwork' . "\t"
    . "" . 'Exam' . "\t" . 'Final' . "\t" . 'Symbol' . "\t" . 'Result' . "\t" . 'Date enrolled' . "\n";
    foreach ($students as $student) {
        $c++;
        $user = $DB->get_record('user', ['id' => $student->userid], '*', MUST_EXIST);
        // $date = dateofenrolment($course->id, $user->id, $context->id);
        $date = enrolment_date($user->id, $context->id);
        echo $c . "\t" . fullname($user) . "\t" . round(get_assignment_finalgrade($course->id, $user->id), 2) . "\t" .
        round(get_digitalssessment_grade($course->id, $user->id), 2) . "\t"
        . "" . round(get_groupwork_finalgrade($course->id, $user->id), 2) . "\t"
        . "" . round(get_exam_mark($course->id, $user->id), 2) . "\t"
        . "" . round(get_course_final_grade($course->id, $user->id), 2) . "\t"
        . "" . get_course_symbol($course->id, $user->id) . "\t"
        . "" . strtoupper(get_course_result($course->id, $user->id)) . "\t"
        . "" . date('d-m-Y H:i:s', $date->timemodified) . "\n";
    }
    readfile($filename);
    exit();
} else {
    /**
     * DATA FROM AN ARCHIVED COURSE
     */
    // HEADERS
    // $filename = 'filename=examvenudata.xls';
    $filename = 'filename=' . $course->shortname . ' student assessments.xls';
    header("Content-Type: application/xls");
    header("Content-Type: application/vnd.ms-excel");
    // header("Content-Type: application/octet-stream");
    header("Content-disposition: attachment; " . basename($filename));

    echo 'Count' . "\t" . 'Student' . "\t" . 'Assignment' . "\t" . "Digital Assessments" . "\t" . 'Groupwork' . "\t"
    . "" . 'Exam' . "\t" . 'Final' . "\t" . 'Symbol' . "\t" . 'Result' . "\t" . 'Date enrolled' . "\n";

    $archives = $DB->get_records('local_acr_archived_grades', ['course' => $course->id]);
    $count = 0;
    foreach ($archives as $archive) {
        $count++;
        $date = dateofenrolment($course->id, $user->id, $context->id);
        $user = $DB->get_record('user', ['id' => $archive->userid]);
        echo $count . "\t" . fullname($user) . "\t" . $archive->asg_grade . "\t" .
        $archive->da_grade . "\t"
        . "" . $archive->ga_grade . "\t"
        . "" . $archive->ex_grade . "\t"
        . "" . $archive->final_grade . "\t"
        . "" . ' ' . "\t"
        . "" . $archive->feedback . "\t"
        . "" . date('d-m-Y H:i:s', $date->timecreated) . "\n";
    }


    //    $table = new html_table();
    //    $table->head = array('Count', 'Student', 'Assignment', 'Digital As', 'Groupwork', 'Exam', 'Final', 'Symbol', 'Result', 'Date enrolled');
    $students = $DB->get_records_sql("SELECT userid FROM {role_assignments} WHERE contextid = ? AND roleid = ? ", [$context->id, 5]);
    $c = 0; // count of each student
    foreach ($students as $student) {
        $c++;
        $user = $DB->get_record('user', ['id' => $student->userid], '*', MUST_EXIST);
        // $date = dateofenrolment($course->id, $user->id, $context->id);
        $date = enrolment_date($user->id, $context->id);
        echo $c . "\t" . fullname($user) . "\t" . round(get_assignment_finalgrade($course->id, $user->id), 2) . "\t" .
        round(get_digitalssessment_grade($course->id, $user->id), 2) . "\t"
        . "" . round(get_groupwork_finalgrade($course->id, $user->id), 2) . "\t"
        . "" . round(get_exam_mark($course->id, $user->id), 2) . "\t"
        . "" . round(get_course_final_grade($course->id, $user->id), 2) . "\t"
        . "" . get_course_symbol($course->id, $user->id) . "\t"
        . "" . strtoupper(get_course_result($course->id, $user->id)) . "\t"
        . "" . date('d-m-Y H:i:s', $date->timemodified) . "\n";
    }
    readfile($filename);
    exit();
}

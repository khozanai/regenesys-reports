<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once($CFG->dirroot . '/local/academicrecord/lib.php');
require_once($CFG->dirroot . '/blocks/archivedata/lib.php');
purge_all_caches();
$id = required_param('id', PARAM_INT); // Course ID

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST); // ... course object
$context = context_course::instance($course->id);
require_login($course);

$PAGE->set_title('Assessments data');
$PAGE->set_pagelayout('standard');
$pageurl = new moodle_url('/blocks/regenesysreports/assessments.php', array(
    'id' => $course->id
        ));
$PAGE->set_url($pageurl);
$PAGE->navbar->add('Students assessments data', $pageurl);
$PAGE->requires->js('/blocks/regenesysreports/reports.js');
echo '<span style="display: none;" class="' . $course->id . '" id="courseid"></span>';
echo $OUTPUT->header();
// fetch all the enrolled students
$students = $DB->get_records_sql("SELECT userid FROM {role_assignments} WHERE contextid = ? AND roleid = ? ", [$context->id, 5]);

if (!archived_course_check_course($course->id)) {

    // Download button
    echo html_writer::empty_tag('br');
    echo html_writer::tag('button', 'Download this data', array('id' => 'download'));
    echo html_writer::empty_tag('br');
    echo html_writer::empty_tag('br');
    // course hasn't been archived

    $table = new html_table();
    $table->head = array('Count', 'Student', 'Assignment', 'Digital As', 'Groupwork', 'Exam', 'Final', 'Symbol', 'Result', 'Date enrolled');
    $c = 0; // count of each student
    foreach ($students as $student) {
        $c++;
        $user = $DB->get_record('user', ['id' => $student->userid], '*', MUST_EXIST);
        // $date = dateofenrolment($course->id, $user->id, $context->id);
        $date = enrolment_date($user->id, $context->id);
        $row = new html_table_row(array(
            $c,
            fullname($user),
            round(get_assignment_finalgrade($course->id, $user->id), 2),
            round(get_digitalssessment_grade($course->id, $user->id), 2),
            round(get_groupwork_finalgrade($course->id, $user->id), 2),
            round(get_exam_mark($course->id, $user->id), 2),
            round(get_course_final_grade($course->id, $user->id), 2),
            get_course_symbol($course->id, $user->id),
            strtoupper(get_course_result($course->id, $user->id)),
            '' // date('d-m-Y H:i:s', $date->timemodified),
        ));
        $row->attributes['class'] = '';
        $table->data[] = $row;
    }

    echo html_writer::table($table);

    echo html_writer::empty_tag('br');
} else {
    /**
     * THIS MODULE HAS BEEN ARCHIVED
     * 
     * 
     * DATA IN THIS MODULE IS COMING FROM ARCHIVES AS WELL AS FROM THE GRADEBOOK
     */
    echo $OUTPUT->notification('Some of the data in this module is coming from the archives');
    echo html_writer::empty_tag('br');

    echo html_writer::tag('button', 'Download', array('id' => 'download_2'));
    echo html_writer::empty_tag('br');
    echo html_writer::empty_tag('br');
    $archives = $DB->get_records('local_acr_archived_grades', ['course' => $course->id]);
    $table = new html_table();
    $table->head = array('Count', 'Student', 'Assignment', 'Digital As', 'Groupwork', 'Exam', 'Final', 'Symbol', 'Result', 'Date enrolled');
    $count = 0;
    foreach ($archives as $archive) {
        $count++;
        $user = $DB->get_record('user', ['id' => $archive->userid]);
        $date = dateofenrolment($course->id, $user->id, $context->id);
        $row = new html_table_row(array(
            $count,
            fullname($user),
            $archive->asg_grade,
            $archive->da_grade,
            $archive->ga_grade,
            $archive->ex_grade,
            $archive->final_grade,
            '',
            $archive->feedback,
            ''    // date('d-m-Y H:i:s', $date->timecreated)
        ));
        $row->attributes['class'] = '';
        $table->data[] = $row;
    }
    // echo html_writer::table($table);

    /**
     * FOLLOWING DATA IS FOR STUDENTS WHO HAVE HAVEN'T PASSED YET
     */
    // $table = new html_table();
    //  $table->head = array('Count', 'Student', 'Assignment', 'Digital As', 'Groupwork', 'Exam', 'Final', 'Symbol', 'Result', 'Date enrolled');


    $c = 0; // count of each student
    foreach ($students as $student) {
        $c++;
        $user = $DB->get_record('user', ['id' => $student->userid]);
        // $date = dateofenrolment($course->id, $user->id, $context->id);
        $date = enrolment_date($user->id, $context->id);
        $row = new html_table_row(array(
            $c,
            fullname($user),
            round(get_assignment_finalgrade($course->id, $user->id), 2),
            round(get_digitalssessment_grade($course->id, $user->id), 2),
            round(get_groupwork_finalgrade($course->id, $user->id), 2),
            round(get_exam_mark($course->id, $user->id), 2),
            round(get_course_final_grade($course->id, $user->id), 2),
            get_course_symbol($course->id, $user->id),
            strtoupper(get_course_result($course->id, $user->id)),
            '' //   date('d-m-Y H:i:s', $date->timemodified)
        ));
        $row->attributes['class'] = '';
        $table->data[] = $row;
    }

    echo html_writer::table($table);
}
echo '<span style="display: none;" class="' . archived_course_check_course($course->id) . '" id="archived"></span>';
echo $OUTPUT->footer();

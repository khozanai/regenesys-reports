<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once '../../../config.php';
require_once '../lib.php';
$quizid = optional_param('quizid', 0, PARAM_INT); // Course ID

$quiz = $DB->get_record('quiz', ['id' => $quizid], '*', MUST_EXIST);
$course = $DB->get_record('course', ['id' => $quiz->course], '*', MUST_EXIST);
$cm = $DB->get_record('course_modules', ['instance' => $quiz->id, 'course' => $course->id, 'module' => 16]);

require_login($course, false, $cm);
echo $OUTPUT->heading($quiz->name . ': student data');

// print_r(fetch_quiz_grades($quizid));
$students = fetch_enrolled_students($course->id);

$table = new html_table();
$table->head = array('Student', 'Done', 'Total');

foreach ($students as $student) {
    $user = $DB->get_record('user', ['id' => $student->userid]);
    $row = new html_table_row(array(
        fullname($user),
        count_digitalassessments_done($user->id, $course->id),
        count_digitalassessments($course->id)
    ));
    $row->attributes['class'] = '';
    $row->attributes['id'] = '';
    $table->data[] = $row;
}
echo html_writer::table($table);

<?php

/**
 * Install script for block_regenesysreports
 *
 * File         install.php
 * Encoding     UTF-8
 *
 * @package     block_regenesysreports
 *
 * @copyright   2017 Raymond Mlambo
 * @author      Raymond Mlambo <khozanai@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

/**
 * Install
 */
function xmldb_block_regenesysreports_install() {
    
}

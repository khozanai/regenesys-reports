<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
$id = optional_param('id', 0, PARAM_INT); // Course ID


$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST); // ... course object
$context = context_course::instance($course->id);
require_login($course);

$PAGE->set_title('Reports: ' . $course->shortname);
$PAGE->set_pagelayout('standard');
$PAGE->set_url(new moodle_url('/blocks/regenesysreports/digitalassessments.php', array(
    'id' => $course->id
)));
$PAGE->navbar->add('Digital assessments reports: ' . $course->shortname, new moodle_url('/blocks/regenesysreports/digitalassessments.php', array(
    'id' => $course->id
)));
$PAGE->requires->js('/blocks/regenesysreports/reports.js');
echo $OUTPUT->header();

echo $OUTPUT->notification('Click on any of the digital assessments below to view the data');
echo html_writer::empty_tag('br');
$das = digitalassessments($course->id);

$table = new html_table();
$table->head = array('Count', 'Fullname', 'Graded students', 'In-completes', 'Action');
$counter = 0;
foreach ($das as $da) {
    $counter++;
    $quiz = $DB->get_record('quiz', ['id' => $da->instance]);
    $row = new html_table_row(array(
        $counter,
        html_writer::tag('span', $quiz->name, array('class' => 'quiz', 'id' => $quiz->id)),
        count_students_with_quizgrades($course->id, $quiz->id),
        count_students_without_quizgrades($course->id, $quiz->id),
        html_writer::tag('button', 'More', array('class' => 'quiz', 'id' => $quiz->id))
    ));
    $row->attributes['class'] = '';
    $row->attributes['id'] = '';
    $table->data[] = $row;
}
echo html_writer::table($table);

/**
 * Display the data
 */
echo html_writer::empty_tag('br');
echo '<div id="data"></div>';




echo $OUTPUT->footer();

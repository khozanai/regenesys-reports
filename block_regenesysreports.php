<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class block_regenesysreports
 *
 * This is the block that will act as the dashboard for all course archived data 
 * Based on available course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_regenesysreports extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_regenesysreports');
    }

    public function specialization() {
        if (isset($this->config)) {
            if (empty($this->config->title)) {
                $this->title = get_string('defaulttitle', 'block_regenesysreports');
            } else {
                $this->title = $this->config->title;
            }
            if (empty($this->config->text)) {
                $this->config->text = get_string('defaulttext', 'block_regenesysreports');
            }
        }
    }

    public function get_content() {
        global $CFG, $DB, $COURSE, $USER, $OUTPUT, $PAGE;

        $capabilities = array(
            'moodle/backup:backupcourse',
            'moodle/category:manage',
            'moodle/course:create',
            'moodle/site:approvecourse',
            'moodle/restore:restorecourse'
        );
        $context = context_course::instance($COURSE->id);
        if ($this->content !== null) {
            return $this->content;
        }
        if (empty($this->instance)) {
            return $this->content;
        }
        $this->content = new stdClass;

        if ($hassiteconfig or has_any_capability($capabilities, $context)) {

            $das_url = new moodle_url('/blocks/regenesysreports/digitalassessments.php', array(
                'id' => $this->page->course->id
            ));
            $assessments_url = new moodle_url('/blocks/regenesysreports/assessments.php', array(// display assessments data fro each students
                'id' => $this->page->course->id
            ));

            $facilitators_url = new moodle_url('/blocks/regenesysreports/facilitators.php', ['id' => $this->page->course->id]);
            $this->content->text .= '<i class="fa fa-table" aria-hidden="true"></i> ';
            $this->content->text .= html_writer::link($facilitators_url, 'Facilitator forums');
            $this->content->text .= '<br />';
            if ($COURSE->id > 1) {
                $this->content->text .= '<i class="fa fa-table" aria-hidden="true"></i> ';
                $this->content->text .= html_writer::link($assessments_url, 'Assessments data');
                $this->content->text .= '<br />';

                $this->content->text .= '<i class="fa fa-user-times" aria-hidden="true"></i> ';
                $this->content->text .= html_writer::link($das_url, get_string('das', 'block_regenesysreports'));
                $this->content->text .= '<br />';
            }
        }
        //$this->content->footer = 'Other links ...';
        return $this->content;
    }

    public function instance_allow_multiple() {
        return true;
    }

}

<?php

/**
 *  block_regenesysreports
 *
 * View archived course data
 * 
 * @package    block_regenesysreports
 * @copyright  2017 Raymond Mlambo (khozanai@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');
$id = required_param('id', PARAM_INT); // Course ID

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST); // ... course object
$context = context_course::instance($course->id);
require_login($course);
$page_url = new moodle_url('/blocks/regenesysreports/facilitators.php', array(
    'id' => $course->id
        ));

$PAGE->set_title('Reports: ' . $course->shortname);
$PAGE->set_pagelayout('standard');
$PAGE->set_url($page_url);
$PAGE->navbar->add('Modules', '/course/index.php?categoryid=' . $course->category);
$PAGE->navbar->add($course->fullname, '/course/view.php?id=' . $course->id);
$PAGE->navbar->add('Facilitator forums data', $page_url);

echo $OUTPUT->header();

$br = html_writer::empty_tag('br');
$programme_forums = $DB->get_records_sql("select u.firstname, u.lastname, u.id from {programforums} p join {user} u on u.id = p.createdby "
        . " order by p.id desc");

$table = new html_table();
$table->head = ['Individual'];

foreach ($programme_forums as $programme_forum) {
    $url = new moodle_url('/blocks/regenesysreports/facilitator_posts.php', ['id' => $programme_forum->id, 'courseid' => $course->id]);
    $row = new html_table_row([
        html_writer::link($url, $programme_forum->firstname . ' ' . $programme_forum->lastname . $br)
    ]);
  $table->data[] = $row;  
}
echo html_writer::table($table);
echo $OUTPUT->footer();
